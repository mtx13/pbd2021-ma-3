package si.uni_lj.fri.pbd.miniapp3.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.AsyncTask
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import si.uni_lj.fri.pbd.miniapp3.R
import si.uni_lj.fri.pbd.miniapp3.models.Mapper
import si.uni_lj.fri.pbd.miniapp3.models.RecipeDetailsIM
import si.uni_lj.fri.pbd.miniapp3.models.RecipeSummaryIM
import si.uni_lj.fri.pbd.miniapp3.models.dto.RecipeSummaryDTO
import si.uni_lj.fri.pbd.miniapp3.ui.DetailsActivity


class RecyclerViewAdapter(context: Context, var from: String) : RecyclerView.Adapter<RecyclerViewAdapter.CardViewHolder>() {

    var mContext: Context? = null
    private var items: List<RecipeSummaryIM>? = null

    fun setItemList(itemsList: List<RecipeSummaryIM>?) {
        items = itemsList
        notifyDataSetChanged()
    }


    inner class CardViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView!!) {
        var itemImage: ImageView? = null
        var itemTitle: TextView? = null

        init {
            itemImage = itemView?.findViewById(R.id.image_view)
            itemTitle = itemView?.findViewById(R.id.text_view_content)
            mContext = itemView?.context
        }
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): CardViewHolder {
        val view = LayoutInflater.from(viewGroup.context).inflate(R.layout.layout_grid_item, viewGroup, false)

        return CardViewHolder(view)
    }

    override fun onBindViewHolder(viewHolder: CardViewHolder, i: Int) {
        viewHolder.itemTitle?.text = items!![i].strDrink
        DownloadImageFromInternet(viewHolder.itemImage!!).execute(items!![i].strDrinkThumb)
        viewHolder.itemImage!!.setOnClickListener {
            val intent = Intent(mContext, DetailsActivity::class.java)
            intent.putExtra("id", items!![i].idDrink)
            intent.putExtra("from", from)
            mContext?.startActivity(intent)
        }
    }

    override fun getItemCount(): Int {
        return if (items == null) 0 else items!!.size
    }

    @SuppressLint("StaticFieldLeak")
    @Suppress("DEPRECATION")
    private inner class DownloadImageFromInternet(var imageView: ImageView) : AsyncTask<String, Void, Bitmap?>() {
        override fun doInBackground(vararg urls: String): Bitmap? {
            val imageURL = urls[0]
            var image: Bitmap? = null
            try {
                val `in` = java.net.URL(imageURL).openStream()
                image = BitmapFactory.decodeStream(`in`)
            }
            catch (e: Exception) {
                Log.e("Error Message", e.message.toString())
                e.printStackTrace()
            }
            return image
        }
        override fun onPostExecute(result: Bitmap?) {
            imageView.setImageBitmap(result)
        }
    }

}