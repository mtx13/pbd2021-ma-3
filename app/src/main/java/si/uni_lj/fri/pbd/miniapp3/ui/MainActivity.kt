package si.uni_lj.fri.pbd.miniapp3.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import si.uni_lj.fri.pbd.miniapp3.R
import si.uni_lj.fri.pbd.miniapp3.databinding.ActivityMainBinding


class MainActivity : AppCompatActivity() {
    var titles: Array<String> = arrayOf("SEARCH BY INGREDIENT", "FAVORITES")
    private lateinit var binding: ActivityMainBinding

    companion object {
        private const val TAG = "MainActivity"
        private const val NUM_OF_TABS = 2
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)

        configureTabLayout()
    }

    private fun configureTabLayout() {

        val tabLayout = findViewById<TabLayout>(R.id.tab_layout)
        val viewPager = findViewById<ViewPager2>(R.id.view_pager)

        viewPager?.adapter = SectionsPagerAdapter(this, NUM_OF_TABS)

        TabLayoutMediator(tabLayout, viewPager) { tab, position ->
            tab.text = titles[position]
            viewPager?.setCurrentItem(tab.position, true)
        }.attach()
    }
}