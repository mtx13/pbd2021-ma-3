package si.uni_lj.fri.pbd.miniapp3.ui.search

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.AdapterView.OnItemSelectedListener
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.databinding.DataBindingUtil.bind
import androidx.databinding.DataBindingUtil.setContentView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import si.uni_lj.fri.pbd.miniapp3.adapter.RecyclerViewAdapter
import si.uni_lj.fri.pbd.miniapp3.adapter.SpinnerAdapter
import si.uni_lj.fri.pbd.miniapp3.databinding.FragmentSearchBinding
import si.uni_lj.fri.pbd.miniapp3.models.Mapper
import si.uni_lj.fri.pbd.miniapp3.models.dto.IngredientDTO
import si.uni_lj.fri.pbd.miniapp3.models.dto.IngredientsDTO
import si.uni_lj.fri.pbd.miniapp3.models.dto.RecipesByIngredientDTO
import si.uni_lj.fri.pbd.miniapp3.rest.RestAPI
import si.uni_lj.fri.pbd.miniapp3.rest.ServiceGenerator
import si.uni_lj.fri.pbd.miniapp3.ui.favorites.MainViewModel


class SearchFragment : Fragment() {

    private var _binding: FragmentSearchBinding? = null
    private val binding get() = _binding!!
    private var layoutManager: RecyclerView.LayoutManager? = null
    private var adapter: RecyclerViewAdapter? = null

    private val service = ServiceGenerator.createService(RestAPI::class.java)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        adapter = RecyclerViewAdapter(context!!, "search")
        getIngredients()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        _binding = FragmentSearchBinding.inflate(inflater, container, false)
        //loading swipe refresh
        binding.swiperefresh.setOnRefreshListener {
            getIngredients()
            binding.swiperefresh.isRefreshing = false
        }
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    //gets ingredients from api and sets them to spinner
    private fun getIngredients() {
        service.allIngredients?.enqueue(object : Callback<IngredientsDTO?> {
            override fun onResponse(call: Call<IngredientsDTO?>, response:
            Response<IngredientsDTO?>) {
                if (response.code() == 200) {
                    binding.spinner.adapter = SpinnerAdapter(context!!, response.body()?.ingredients!!)

                    binding.spinner.onItemSelectedListener = object : OnItemSelectedListener {
                        override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                            val item = parent.selectedItem as IngredientDTO
                            getCocktails(item.strIngredient1!!)
                        }

                        override fun onNothingSelected(parent: AdapterView<*>?) {}
                    }
                } else Toast.makeText(context, "Ingredients could not be loaded", Toast.LENGTH_LONG).show();
            }

            override fun onFailure(call: Call<IngredientsDTO?>, t: Throwable) {
                Toast.makeText(context, "Ingredients could not be loaded", Toast.LENGTH_LONG).show();
            }
        })
    }

    //cocktails from api for id
    private fun getCocktails(item: String) {
        //spinner start loading animation
        binding.loadingSpinner.isVisible = true
        service.recipesByIngredient(item)?.enqueue(object : Callback<RecipesByIngredientDTO?> {
            override fun onResponse(call: Call<RecipesByIngredientDTO?>, response:
            Response<RecipesByIngredientDTO?>) {
                if (response.code() == 200) {
                    binding.loadingSpinner.isVisible = false
                    binding.recyclerView.layoutManager = LinearLayoutManager(view?.context)
                    binding.recyclerView.adapter = adapter

                    val drinks = response.body()?.drinks!!
                    adapter?.setItemList(drinks.map{Mapper.mapRecipeSummaryDTOToRecipeSummaryIm(it)})
                } else Toast.makeText(context, "Cocktails could not be loaded", Toast.LENGTH_LONG).show();
            }

            override fun onFailure(call: Call<RecipesByIngredientDTO?>, t: Throwable) {
                Toast.makeText(context, "Cocktails could not be loaded", Toast.LENGTH_LONG).show();
            }
        })
    }
}