package si.uni_lj.fri.pbd.miniapp3.ui

import android.annotation.SuppressLint
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.ImageView
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import si.uni_lj.fri.pbd.miniapp3.adapter.RecyclerViewAdapter
import si.uni_lj.fri.pbd.miniapp3.database.entity.RecipeDetails
import si.uni_lj.fri.pbd.miniapp3.databinding.ActivityDetailsBinding
import si.uni_lj.fri.pbd.miniapp3.models.Mapper
import si.uni_lj.fri.pbd.miniapp3.models.RecipeDetailsIM
import si.uni_lj.fri.pbd.miniapp3.models.dto.RecipesByIdDTO
import si.uni_lj.fri.pbd.miniapp3.rest.RestAPI
import si.uni_lj.fri.pbd.miniapp3.rest.ServiceGenerator
import si.uni_lj.fri.pbd.miniapp3.ui.favorites.MainViewModel
import java.util.*

class DetailsActivity : AppCompatActivity() {

    private val service = ServiceGenerator.createService(RestAPI::class.java)
    private lateinit var binding: ActivityDetailsBinding
    private var mViewModel: MainViewModel? = null

    private var isFavorite = false


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetailsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        mViewModel = ViewModelProvider(this).get(MainViewModel::class.java)
        observerSetup()

        //switching between fragments
        when(intent.getStringExtra("from")) {
            "search" -> getDetailsApi(intent.getStringExtra("id")!!)
            "favorites" -> getDetailsLocal(intent.getStringExtra("id")!!)
        }
    }

    //gets details based on passed id
    private fun getDetailsApi(id: String) {
        service.recipesById(id)?.enqueue(object : Callback<RecipesByIdDTO?> {
            override fun onResponse(call: Call<RecipesByIdDTO?>, response:
            Response<RecipesByIdDTO?>) {
                if (response.code() == 200) {
                    val recipe = response.body()?.drinks!!
                    val recipeIM = Mapper.mapRecipeDetailsDtoToRecipeDetailsIm(false, recipe[0])

                    setDetails(recipeIM)
                    //favorite button
                    mViewModel?.searchResults?.observe(this@DetailsActivity, { recipes ->
                        if (recipes.isNotEmpty()) {
                            binding.favorite.text = "unfavorite"
                        }
                        else {
                            binding.favorite.text = "favorite"
                        }
                    })
                    mViewModel?.findCocktail(id)
                    binding.favorite.setOnClickListener {
                        if(binding.favorite.text == "favorite") {
                            mViewModel?.insertCocktail(Mapper.mapRecipeDetailsDtoToRecipeDetails(true, recipe[0]))
                            binding.favorite.text = "unfavorite"
                        }
                        else {
                            mViewModel?.deleteCocktail(id)
                            binding.favorite.text = "favorite"
                        }
                    }

                } else Toast.makeText(applicationContext, "Details could not be loaded", Toast.LENGTH_LONG).show();
            }

            override fun onFailure(call: Call<RecipesByIdDTO?>, t: Throwable) {
                Toast.makeText(applicationContext, "Details could not be loaded", Toast.LENGTH_LONG).show();
            }
        })
    }

    //get details if activity is called from favorites fragment
    private fun getDetailsLocal(id: String) {
        mViewModel?.findCocktail(id)
        binding.favorite.text = "unfavorite"
        binding.favorite.setOnClickListener {
            mViewModel?.deleteCocktail(id)
        }
    }

    //formats data and sets it
    private fun setDetails(recipe: RecipeDetailsIM) {
        DownloadImageFromInternet(binding.imageView).execute(recipe.strDrinkThumb)
        binding.name.text = recipe.strDrink
        binding.ingredients.text = formatIngredients(recipe)
        binding.measures.text = formatMeasures(recipe)
        binding.instructions.text = recipe.strInstructions
    }

    //updates data from database
    private fun observerSetup() {
        mViewModel?.searchResults?.observe(this@DetailsActivity, { recipes ->
            if (recipes.isNotEmpty()) {
                setDetails(Mapper.mapRecipeDetailsToRecipeDetailsIm(true, recipes[0]))
                isFavorite = true
            }
            else {
                isFavorite = false
            }
        })
    }

    //formating for details
    private fun formatIngredients(recipe: RecipeDetailsIM) : String? {
        var ingredients = recipe.strIngredient1
        if(recipe.strIngredient1 != null) ingredients = ("${ingredients}, ${recipe.strIngredient1}")
        if(recipe.strIngredient2 != null) ingredients = ("${ingredients}, ${recipe.strIngredient2}")
        if(recipe.strIngredient3 != null) ingredients = ("${ingredients}, ${recipe.strIngredient3}")
        if(recipe.strIngredient4 != null) ingredients = ("${ingredients}, ${recipe.strIngredient4}")
        if(recipe.strIngredient5 != null) ingredients = ("${ingredients}, ${recipe.strIngredient5}")
        if(recipe.strIngredient6 != null) ingredients = ("${ingredients}, ${recipe.strIngredient6}")
        if(recipe.strIngredient7 != null) ingredients = ("${ingredients}, ${recipe.strIngredient7}")
        if(recipe.strIngredient8 != null) ingredients = ("${ingredients}, ${recipe.strIngredient8}")
        if(recipe.strIngredient9 != null) ingredients = ("${ingredients}, ${recipe.strIngredient9}")
        if(recipe.strIngredient10 != null) ingredients = ("${ingredients}, ${recipe.strIngredient10}")
        if(recipe.strIngredient11 != null) ingredients = ("${ingredients}, ${recipe.strIngredient11}")
        if(recipe.strIngredient12 != null) ingredients = ("${ingredients}, ${recipe.strIngredient12}")
        if(recipe.strIngredient13 != null) ingredients = ("${ingredients}, ${recipe.strIngredient13}")
        if(recipe.strIngredient14 != null) ingredients = ("${ingredients}, ${recipe.strIngredient14}")
        if(recipe.strIngredient15 != null) ingredients = ("${ingredients}, ${recipe.strIngredient15}")
        return ingredients
    }

    private fun formatMeasures(recipe: RecipeDetailsIM) : String? {
        var ingredients = recipe.strMeasure1
        if(recipe.strMeasure1 != null) ingredients = ("${ingredients}, ${recipe.strMeasure1}")
        if(recipe.strMeasure2 != null) ingredients = ("${ingredients}, ${recipe.strMeasure2}")
        if(recipe.strMeasure3 != null) ingredients = ("${ingredients}, ${recipe.strMeasure3}")
        if(recipe.strMeasure4 != null) ingredients = ("${ingredients}, ${recipe.strMeasure4}")
        if(recipe.strMeasure5 != null) ingredients = ("${ingredients}, ${recipe.strMeasure5}")
        if(recipe.strMeasure6 != null) ingredients = ("${ingredients}, ${recipe.strMeasure6}")
        if(recipe.strMeasure7 != null) ingredients = ("${ingredients}, ${recipe.strMeasure7}")
        if(recipe.strMeasure8 != null) ingredients = ("${ingredients}, ${recipe.strMeasure8}")
        if(recipe.strMeasure9 != null) ingredients = ("${ingredients}, ${recipe.strMeasure9}")
        if(recipe.strMeasure10 != null) ingredients = ("${ingredients}, ${recipe.strMeasure10}")
        if(recipe.strMeasure11 != null) ingredients = ("${ingredients}, ${recipe.strMeasure11}")
        if(recipe.strMeasure12 != null) ingredients = ("${ingredients}, ${recipe.strMeasure12}")
        if(recipe.strMeasure13 != null) ingredients = ("${ingredients}, ${recipe.strMeasure13}")
        if(recipe.strMeasure14 != null) ingredients = ("${ingredients}, ${recipe.strMeasure14}")
        if(recipe.strMeasure15 != null) ingredients = ("${ingredients}, ${recipe.strMeasure15}")
        return ingredients
    }

    //used to download image from url link
    @SuppressLint("StaticFieldLeak")
    @Suppress("DEPRECATION")
    private inner class DownloadImageFromInternet(var imageView: ImageView) : AsyncTask<String, Void, Bitmap?>() {
        override fun doInBackground(vararg urls: String): Bitmap? {
            val imageURL = urls[0]
            var image: Bitmap? = null
            try {
                val `in` = java.net.URL(imageURL).openStream()
                image = BitmapFactory.decodeStream(`in`)
            }
            catch (e: Exception) {
                Log.e("Error Message", e.message.toString())
                e.printStackTrace()
            }
            return image
        }
        override fun onPostExecute(result: Bitmap?) {
            imageView.setImageBitmap(result)
        }
    }
}