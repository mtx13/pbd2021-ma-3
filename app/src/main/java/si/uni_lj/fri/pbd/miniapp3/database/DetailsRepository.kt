package si.uni_lj.fri.pbd.miniapp3.database

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import si.uni_lj.fri.pbd.miniapp3.database.dao.RecipeDao
import si.uni_lj.fri.pbd.miniapp3.database.entity.RecipeDetails

class DetailsRepository(application: Application?) {
    val searchResults = MutableLiveData<List<RecipeDetails>>()
    val allProducts: LiveData<List<RecipeDetails>>

    private val productDao: RecipeDao

    fun insertProduct(newproduct: RecipeDetails) {
        Database.databaseWriteExecutor.execute(Runnable {
            productDao.insertProduct(newproduct)
        })

    }

    fun deleteProduct(name: String) {
        Database.databaseWriteExecutor.execute(Runnable {
            productDao.deleteProduct(name)
        })

    }

    fun findProduct(id: String) {
        Database.databaseWriteExecutor.execute(Runnable {
            searchResults.postValue(productDao.getRecipeById(id))
        })
    }


    init {
        val db: Database =
                Database.getDatabase(application?.applicationContext!!)!!
        productDao = db.recipeDao()
        allProducts = productDao.allProducts
    }
}