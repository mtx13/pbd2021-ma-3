package si.uni_lj.fri.pbd.miniapp3.ui.favorites

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import si.uni_lj.fri.pbd.miniapp3.adapter.RecyclerViewAdapter
import si.uni_lj.fri.pbd.miniapp3.databinding.FragmentFavoritesBinding
import si.uni_lj.fri.pbd.miniapp3.models.Mapper


class FavoritesFragment: Fragment() {

    private var _binding: FragmentFavoritesBinding? = null
    private val binding get() = _binding!!
    private var adapter: RecyclerViewAdapter? = null
    private var mViewModel: MainViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mViewModel = ViewModelProvider(this).get(MainViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        _binding= FragmentFavoritesBinding.inflate(inflater, container, false)

        adapter = RecyclerViewAdapter(context!!, "favorites")
        binding.recyclerView.layoutManager = LinearLayoutManager(view?.context)
        binding.recyclerView.adapter = adapter

        getCocktails()
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    //retrieves all cocktails from local database
    private fun getCocktails() {
        mViewModel?.allCocktails!!.observe(viewLifecycleOwner) { cocktails ->
            adapter?.setItemList(cocktails.map { Mapper.mapRecipeDetailsToRecipeSummaryIm(it)} )
        }
    }
}