package si.uni_lj.fri.pbd.miniapp3.models.dto

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class RecipeSummaryDTO(
        @field:Expose @field:SerializedName("strDrink") val strDrink: String,
        @field:Expose @field:SerializedName("strDrinkThumb") val strDrinkThumb: String,
        @field:Expose @field:SerializedName("idDrink") val idDrink: String
)