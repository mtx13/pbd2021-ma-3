package si.uni_lj.fri.pbd.miniapp3.ui.favorites

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import si.uni_lj.fri.pbd.miniapp3.database.DetailsRepository
import si.uni_lj.fri.pbd.miniapp3.database.entity.RecipeDetails

class MainViewModel(application: Application?) : AndroidViewModel(application!!) {
    var allCocktails: LiveData<List<RecipeDetails>>
    var searchResults: MutableLiveData<List<RecipeDetails>>

    private val repository: DetailsRepository

    fun insertCocktail(product: RecipeDetails) {
        repository.insertProduct(product)
    }

    fun findCocktail(id: String) {
        repository.findProduct(id)
    }

    fun deleteCocktail(name: String) {
        repository.deleteProduct(name)
    }

    init {
        repository = DetailsRepository(application)
        allCocktails = repository.allProducts
        searchResults = repository.searchResults
    }
}