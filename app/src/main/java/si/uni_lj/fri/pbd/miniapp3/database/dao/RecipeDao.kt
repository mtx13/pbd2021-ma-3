package si.uni_lj.fri.pbd.miniapp3.database.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import si.uni_lj.fri.pbd.miniapp3.database.entity.RecipeDetails

@Dao
interface RecipeDao {
    @Query("SELECT * FROM RecipeDetails WHERE idDrink = :idDrink")
    fun getRecipeById(idDrink: String?): List<RecipeDetails>

    // TODO: Add the missing methods
    @get:Query("SELECT * FROM RecipeDetails")
    val allProducts: LiveData<List<RecipeDetails>>

    @Insert
    fun insertProduct(product: RecipeDetails)

    @Query("DELETE FROM RecipeDetails WHERE idDrink = :idDrink")
    fun deleteProduct(idDrink: String?)

}